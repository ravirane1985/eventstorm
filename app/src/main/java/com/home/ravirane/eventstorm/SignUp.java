package com.home.ravirane.eventstorm;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class SignUp extends ActionBarActivity {

    Button btnSignUp;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        btnSignUp = (Button) findViewById(R.id.btnSignUp2);


        btnSignUp.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {
                                             SQLiteDatabase sqLitedb = ((Activity) v.getContext()).openOrCreateDatabase("eventDb",MODE_PRIVATE,null);
                                             EditText txtUser = (EditText) findViewById(R.id.txtUserName2);
                                             EditText txtPass = (EditText) findViewById(R.id.txtPassword2);
                                             EditText txtMailId = (EditText) findViewById(R.id.txtMailId);
                                             String createUsr = "CREATE TABLE IF NOT EXISTS tblUser(userName VARCHAR PRIMARY KEY, password VARCHAR, mailId VARCHAR)";
                                             sqLitedb.execSQL(createUsr);
                                             String insertData = "INSERT INTO tblUser VALUES('" + txtUser.getText().toString() + "','" + txtPass.getText().toString() + "','" + txtMailId.getText().toString() + "')";
                                             Log.e(insertData,insertData);
                                             sqLitedb.execSQL(insertData);
                                             Intent intent = new Intent(SignUp.this, Login.class);
                                             SignUp.this.startActivity(intent);
                                             sqLitedb.close();
                                         }
                                     }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
