package com.home.ravirane.util;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.home.ravirane.eventstorm.R;
import com.home.ravirane.model.EventType;

import java.util.List;

/**
 * Created by ravirane on 7/12/15.
 */
public class EventTypeAdapter extends BaseAdapter {


    private Activity activity;
    private List<EventType> dataList;
    private static LayoutInflater inflater=null;
//    public ImageLoader imageLoader;


    public EventTypeAdapter(Activity activity, List<EventType> dataList) {
        this.activity = activity;
        this.dataList = dataList;
        inflater = activity.getLayoutInflater();
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.event_type_row, null);

        TextView eventType = (TextView)vi.findViewById(R.id.eventType); // title
        TextView eventTypeDetails = (TextView)vi.findViewById(R.id.eventTypeDetails); // artist name
        TextView eventCount = (TextView)vi.findViewById(R.id.eventCounts); // duration
        ImageView evenTypeImg=(ImageView)vi.findViewById(R.id.eventTypeImg); // thumb image

        EventType eType = dataList.get(position);

        eventType.setText(eType.getEventType());
        eventTypeDetails.setText(eType.getEventTypeDetails());
        eventCount.setText(Integer.toString(eType.getEvenCount()));

        evenTypeImg.setTag(eType.getEventTypeUrl());
        new LoadImageTask().execute(evenTypeImg);

        return vi;
    }
}
