package com.home.ravirane.util;

import android.app.Application;

/**
 * Created by ravirane on 7/14/15.
 */
public class MyApplication extends Application {

    private String eventType;

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
}
