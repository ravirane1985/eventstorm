package com.home.ravirane.util;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.home.ravirane.eventstorm.R;
import com.home.ravirane.model.Event;

import java.util.List;

/**
 * Created by ravirane on 7/12/15.
 */
public class EventAdapter extends BaseAdapter {

    private Activity activity;
    private List<Event> eventList;
    private static LayoutInflater inflater=null;

    public EventAdapter(Activity activity, List<Event> eventList) {

        this.activity = activity;
        this.eventList = eventList;
        inflater = activity.getLayoutInflater();
    }

    @Override
    public int getCount() {
        return eventList.size();
    }

    @Override
    public Object getItem(int position) {
        return eventList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.event_row, null);

        TextView eventTitle = (TextView)vi.findViewById(R.id.event); // title
        TextView eventShortDetails = (TextView)vi.findViewById(R.id.eventDetails); // artist name
        TextView eventCost = (TextView)vi.findViewById(R.id.eventCost); // duration
        ImageView eventThumb=(ImageView)vi.findViewById(R.id.eventThumb); // thumb image

        Event event = eventList.get(position);

        eventTitle.setText(event.getEventTitle());
        eventShortDetails.setText(event.getEventShortDetails());
        eventCost.setText(event.getEventCost());
        eventThumb.setTag((String) event.getEventDPPic());

        new LoadImageTask().execute(eventThumb);
        return vi;
    }
}
