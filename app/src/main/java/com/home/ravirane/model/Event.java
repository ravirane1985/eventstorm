package com.home.ravirane.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ravirane on 7/12/15.
 */
public class Event implements Serializable {

    private String eventTitle;
    private String eventShortDetails;
    private String eventLongDetails;
    private String eventDPPic;
    private String eventThumbPic;
    private String eventCost;
    private String eventDate;
    private List<String> eventAdditionalPics;


    public String getEventThumbPic() {
        return eventThumbPic;
    }

    public void setEventThumbPic(String eventThumbPic) {
        this.eventThumbPic = eventThumbPic;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getEventShortDetails() {
        return eventShortDetails;
    }

    public void setEventShortDetails(String eventShortDetails) {
        this.eventShortDetails = eventShortDetails;
    }

    public String getEventLongDetails() {
        return eventLongDetails;
    }

    public void setEventLongDetails(String eventLongDetails) {
        this.eventLongDetails = eventLongDetails;
    }

    public String getEventDPPic() {
        return eventDPPic;
    }

    public void setEventDPPic(String eventDPPic) {
        this.eventDPPic = eventDPPic;
    }

    public String getEventCost() {
        return eventCost;
    }

    public void setEventCost(String eventCost) {
        this.eventCost = eventCost;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public List<String> getEventAdditionalPics() {
        return eventAdditionalPics;
    }

    public void setEventAdditionalPics(List<String> eventAdditionalPics) {
        this.eventAdditionalPics = eventAdditionalPics;
    }
}
