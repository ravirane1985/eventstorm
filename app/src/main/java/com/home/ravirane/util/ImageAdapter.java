package com.home.ravirane.util;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by ravirane on 7/13/15.
 */
public class ImageAdapter extends PagerAdapter {

    Context context;

    private List<String> imgString;

    public ImageAdapter(Context context, List<String> imgString) {
        this.context = context;
        this.imgString = imgString;
    }

    @Override
    public int getCount() {
        return imgString.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);
       /* int padding = context.getResources().getDimensionPixelSize(R.dimen.padding_medium);
        imageView.setPadding(padding, padding, padding, padding);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);*/
       // imageView.setImageResource(galImg[position]);
        imageView.setTag(imgString.get(position));
        new LoadImageTask().execute(imageView);
        ((ViewPager) container).addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }
}
