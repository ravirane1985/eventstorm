package com.home.ravirane.model;

/**
 * Created by ravirane on 7/12/15.
 */
public class EventType {

    private String eventType;
    private String eventTypeDetails;
    private int evenCount;
    private String eventTypeUrl;

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventTypeDetails() {
        return eventTypeDetails;
    }

    public void setEventTypeDetails(String eventTypeDetails) {
        this.eventTypeDetails = eventTypeDetails;
    }

    public int getEvenCount() {
        return evenCount;
    }

    public void setEvenCount(int evenCount) {
        this.evenCount = evenCount;
    }

    public String getEventTypeUrl() {
        return eventTypeUrl;
    }

    public void setEventTypeUrl(String eventTypeUrl) {
        this.eventTypeUrl = eventTypeUrl;
    }
}
