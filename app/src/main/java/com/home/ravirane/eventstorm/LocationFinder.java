package com.home.ravirane.eventstorm;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.home.ravirane.util.HTTPConnect;

public class LocationFinder extends ActionBarActivity implements OnItemSelectedListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_finder);

        Spinner dropdown = (Spinner)findViewById(R.id.btnCountry);
        String[] items = new String[]{"Select Country","India", "Singapore"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        dropdown.setAdapter(adapter);
        dropdown.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.show();
        String ctry = null;
        if(position == 1) {
            ctry = "india";
            new HttpAsyncTask().execute("http://ec2-54-169-98-125.ap-southeast-1.compute.amazonaws.com:8000/eventtype",ctry);
        } else if( position == 2) {
            ctry = "singapore";
            new HttpAsyncTask().execute("http://ec2-54-169-98-125.ap-southeast-1.compute.amazonaws.com:8000/eventtype",ctry);
        }
        progress.dismiss();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_location_finder, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            HTTPConnect connect = new HTTPConnect();
            String jsonResp = connect.GET(urls[0]);
            Intent intent = new Intent(LocationFinder.this, EventTypeSelection.class);
            intent.putExtra("country",urls[1]);
            intent.putExtra("jsonResp",jsonResp);
            Log.e(jsonResp,jsonResp);
            LocationFinder.this.startActivity(intent);
            return jsonResp;
        }
    }
}
