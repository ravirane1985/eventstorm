package com.home.ravirane.eventstorm;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class Login extends ActionBarActivity {


    Button btnLogin;
    Button btnSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);

        btnLogin.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            SQLiteDatabase sqLitedb = ((Activity) v.getContext()).openOrCreateDatabase("eventDb",MODE_PRIVATE,null);
                                            EditText txtUser = (EditText) findViewById(R.id.txtUserName);
                                            EditText txtPass = (EditText) findViewById(R.id.txtPassword);

                                            String userName = txtUser.getText().toString();
                                            String passwrd = txtPass.getText().toString();

                                            Log.e(userName,userName);
                                            Log.e(passwrd, passwrd);

                                            String selectData = "SELECT count(*) from tblUser where userName = '" + userName + "' and password = '" + passwrd + "'";
                                            Log.e(selectData, selectData);
                                            Cursor crsr = sqLitedb.rawQuery(selectData, null);
                                            if(crsr != null) {
                                                if(crsr.moveToFirst()) {
                                                    Intent intent = new Intent(Login.this, LocationFinder.class);
                                                    Login.this.startActivity(intent);
                                                    sqLitedb.close();
                                                } else {

                                                }
                                            } else {

                                            }
                                            sqLitedb.close();
                                        }
                                    }
        );

        btnSignUp.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(Login.this, SignUp.class);
                                            Login.this.startActivity(intent);
                                        }
                                    }
        );


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
