package com.home.ravirane.eventstorm;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.home.ravirane.model.EventType;
import com.home.ravirane.util.EventTypeAdapter;
import com.home.ravirane.util.HTTPConnect;
import com.home.ravirane.util.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class EventTypeSelection extends ActionBarActivity{


    String eventType = "{\"eventType\": [{\"name\":\"Travel\",\"details\":\"Click here and pack your bags\",\"eventImg\":\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436685595/travelsmall_bh9lvk.png\",\"count\":\"15\"},{\"name\":\"Concert\",\"details\":\"Check out latest concert happening around you\",\"eventImg\":\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436685595/travelsmall_bh9lvk.png\",\"count\":\"20\"},{\"name\":\"Sports\",\"details\":\"One stop shop for all sport activities\",\"eventImg\":\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436685595/travelsmall_bh9lvk.png\",\"count\":\"12\"},{\"name\":\"Adventures\",\"details\":\"Adventures you must join for\",\"eventImg\":\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436685595/travelsmall_bh9lvk.png\",\"count\":\"18\"} ]}";

    ListView eventTypeListView;
    EventTypeAdapter eventTypeAdapter;
    Toolbar toolbar;
    private ArrayList<EventType> eventTypes = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_type_selection);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

       if(((MyApplication) this.getApplication()).getEventType() == null) {
           Intent intent = getIntent();
           String resp = intent.getStringExtra("jsonResp");
           if(resp != null) {
               eventType = resp;
           }
       } else {
           eventType = ((MyApplication) this.getApplication()).getEventType();
       }

        eventTypes = new ArrayList<>();
        try {
            JSONObject jsonRootObject = new JSONObject(eventType);
            JSONArray jsonArray = jsonRootObject.optJSONArray("eventType");
            for(int i = 0; i < jsonArray.length(); i++ ) {
                EventType eType = new EventType();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                eType.setEventType(jsonObject.optString("name"));
                eType.setEventTypeDetails(jsonObject.optString("details"));
                eType.setEvenCount(jsonObject.optInt("count"));
                eType.setEventTypeUrl(jsonObject.optString("eventImg"));
                eventTypes.add(eType);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        eventTypeListView=(ListView)findViewById(android.R.id.list);
        eventTypeAdapter = new EventTypeAdapter(this,eventTypes);
        eventTypeListView.setAdapter(eventTypeAdapter);


        eventTypeListView.setClickable(true);

        eventTypeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("Position :", Integer.toString(position));
                EventType eventType = (EventType) ((ListView) parent).getAdapter().getItem(position);
                String eventKey = (eventType.getEventType()).trim().toLowerCase();
                new HttpAsyncTask().execute("http://ec2-54-169-98-125.ap-southeast-1.compute.amazonaws.com:8000/event/" +eventKey+ "/india",eventKey);
            }
        });


        getSupportActionBar().setTitle("Event Categories");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_type_selection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == R.id.country_settings) {
            Intent intent = new Intent(EventTypeSelection.this, LocationFinder.class);
            EventTypeSelection.this.startActivity(intent);
        }
        if(id == R.id.country_settings) {
            Intent intent = new Intent(EventTypeSelection.this, Login.class);
            EventTypeSelection.this.startActivity(intent);
        }
        if(id == R.id.exit_app) {
            System.exit(0);
        }
        return super.onOptionsItemSelected(item);
    }


    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            HTTPConnect connect = new HTTPConnect();
            String jsonResp = connect.GET(urls[0]);
            Intent intent = new Intent(EventTypeSelection.this, EventSelection.class);
            intent.putExtra("eventKey", urls[1]);
            intent.putExtra("jsonResp",jsonResp);
            Log.e(jsonResp, jsonResp);
            EventTypeSelection.this.startActivity(intent);
            return jsonResp;
        }
    }

}
