package com.home.ravirane.eventstorm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.home.ravirane.model.Event;
import com.home.ravirane.util.EventAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class EventSelection extends ActionBarActivity {

    ListView eventListView;
    EventAdapter eventAdapter;
    Toolbar toolbar;
    String eventString = "{\"event\":[{\"name\":\"Trip to Singapore\",\"eventDate\":\"20-Jul-2016\",\"shortDetails\":\"7 Days 6 Night trip to SG\",\"cost\":\"SGD 320$\",\"eventDPPic\":\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436690471/singaporesmall_jkanno.png\",\"eventAdditionalPics\":[\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436768040/singapore_rqdh0j.jpg\",\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767999/singapore4_sozhjb.jpg\",\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767996/singapore3_hx8apl.jpg\",\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767992/singapore2_z8kwo2.jpg\",\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767990/orchard5_zm1rmh.jpg\"],\"eventLongDetails\":\"Singapore, an island city-state off southern Malaysia, is a global financial centre with a tropical climate and multicultural population. In circa-1820 Chinatown stands the red-and-gold Buddha’s Tooth Relic Temple, Little India offers colorful souvenirs and Arab Street is lined with fabric shops\"},{\"name\":\"Trip to Singapore\",\"eventDate\":\"2-Aug-2016\",\"shortDetails\":\"7 Days 6 Night trip to SG\",\"cost\":\"SGD 320$\",\"eventDPPic\":\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436690471/singaporesmall_jkanno.png\",\"eventAdditionalPics\":[\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436768040/singapore_rqdh0j.jpg\",\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767999/singapore4_sozhjb.jpg\",\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767996/singapore3_hx8apl.jpg\",\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767992/singapore2_z8kwo2.jpg\",\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767990/orchard5_zm1rmh.jpg\"],\"eventLongDetails\":\"Singapore, an island city-state off southern Malaysia, is a global financial centre with a tropical climate and multicultural population. In circa-1820 Chinatown stands the red-and-gold Buddha’s Tooth Relic Temple, Little India offers colorful souvenirs and Arab Street is lined with fabric shops\"},{\"name\":\"Trip to Singapore\",\"eventDate\":\"20-Aug-2016\",\"shortDetails\":\"7 Days 6 Night trip to SG\",\"cost\":\"FREE\",\"eventDPPic\":\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436690471/singaporesmall_jkanno.png\",\"eventAdditionalPics\":[\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436768040/singapore_rqdh0j.jpg\",\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767999/singapore4_sozhjb.jpg\",\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767996/singapore3_hx8apl.jpg\",\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767992/singapore2_z8kwo2.jpg\",\"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767990/orchard5_zm1rmh.jpg\"],\"eventLongDetails\":\"Singapore, an island city-state off southern Malaysia, is a global financial centre with a tropical climate and multicultural population. In circa-1820 Chinatown stands the red-and-gold Buddha’s Tooth Relic Temple, Little India offers colorful souvenirs and Arab Street is lined with fabric shops\"}]}";
    ArrayList<Event> eventList = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_selection);

        Intent intent = getIntent();
        String eventName = intent.getStringExtra("eventKey");


        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        Intent intnt = getIntent();
        String resp = intnt.getStringExtra("jsonResp");
        if(resp != null) {
            eventString = resp;
        }

        eventList = getEventList(eventString);
        eventListView=(ListView)findViewById(android.R.id.list);
        eventAdapter = new EventAdapter(this,eventList);
        eventListView.setAdapter(eventAdapter);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Events");

        eventListView.setClickable(true);

        eventListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("Position :", Integer.toString(position));
                Event event = (Event) ((ListView) parent).getAdapter().getItem(position);
                Intent intent = new Intent(EventSelection.this, EventDetails.class);
                intent.putExtra("event",event);
                EventSelection.this.startActivity(intent);

            }
        });

    }

    private ArrayList<Event> getEventList(String eventString) {
        ArrayList<Event> eventList = new ArrayList<>();
        try {
            JSONObject jsonRootObject = new JSONObject(eventString);
            JSONArray jsonArray = jsonRootObject.optJSONArray("event");
            for(int i = 0; i < jsonArray.length(); i++ ) {
                Event event = new Event();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                event.setEventTitle(jsonObject.optString("name"));
                event.setEventShortDetails(jsonObject.optString("shortDetails"));
                event.setEventLongDetails(jsonObject.optString("eventLongDetails"));
                event.setEventCost(jsonObject.optString("cost"));
                event.setEventDPPic(jsonObject.optString("eventDPPic"));
                event.setEventDate(jsonObject.optString("eventDate"));
                event.setEventAdditionalPics(getAdditionalPic(jsonObject));
                eventList.add(event);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return eventList;
    }

    private List<String> getAdditionalPic(JSONObject jsonObject) {
        List<String> picList = new ArrayList<>();
        JSONArray jsonArray = jsonObject.optJSONArray("eventAdditionalPics");
        for(int i = 0; i < jsonArray.length(); i++) {
            try {
                picList.add (jsonArray.getString(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return picList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_selection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
        }
        if(id == R.id.country_settings) {
            Intent intent = new Intent(EventSelection.this, LocationFinder.class);
            EventSelection.this.startActivity(intent);
        }
        if(id == R.id.country_settings) {
            Intent intent = new Intent(EventSelection.this, Login.class);
            EventSelection.this.startActivity(intent);
        }
        if(id == R.id.exit_app) {
            System.exit(0);
        }

        return super.onOptionsItemSelected(item);
    }
}
