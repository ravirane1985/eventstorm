package com.home.ravirane.eventstorm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.home.ravirane.model.Event;
import com.home.ravirane.util.ImageAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class EventDetails extends ActionBarActivity {

    private List<String> imgString = new ArrayList<>();

    String[] mobileArray = {"Trip To Singapore","26-Jun-2016","This is 6 Days 7 Night trip to Singapore with 5 star stay","Singapore, an island city-state off southern Malaysia, is a global financial centre with a tropical climate and multicultural population. In circa-1820 Chinatown stands the red-and-gold Buddha’s Tooth Relic Temple, Little India offers colorful souvenirs and Arab Street is lined with fabric shops"};
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        Intent intent = getIntent();
        Event event = (Event) intent.getSerializableExtra("event");

        if(event.getEventAdditionalPics() != null && event.getEventAdditionalPics().size() > 0) {
            imgString = event.getEventAdditionalPics();
        } else {
            populateImgString();
        }

        List<String> dataList = new ArrayList<>();
        if(event.getEventTitle() != null) {
            dataList.add(event.getEventTitle());
        }
        if(event.getEventDate() != null) {
            dataList.add(event.getEventDate());
        }
        if(event.getEventShortDetails() != null) {
            dataList.add(event.getEventShortDetails());
        }
        if(event.getEventLongDetails() != null) {
            dataList.add(event.getEventLongDetails());
        }
        if(event.getEventCost() != null) {
            Button btnCost = (Button)findViewById(R.id.btnCost);
            btnCost.setText(event.getEventCost());
        }

        if(dataList.size() > 0) {
            Object[] objectList = dataList.toArray();
            mobileArray = Arrays.copyOf(objectList, objectList.length, String[].class);
        }


        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        ImageAdapter adapter = new ImageAdapter(this,imgString);
        viewPager.setAdapter(adapter);

        ArrayAdapter arryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mobileArray);
        ListView listView = (ListView) findViewById(R.id.eventDetailsList);
        listView.setAdapter(arryAdapter);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        Button btnJoin = (Button) findViewById(R.id.btnJoin);
        btnJoin.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {
                                             Intent intent = new Intent(EventDetails.this, Login.class);
                                             EventDetails.this.startActivity(intent);
                                         }
                                     }
        );
        Button btnInterested = (Button) findViewById(R.id.btnInterested);
        btnInterested.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {
                                             Intent intent = new Intent(EventDetails.this, Login.class);
                                             EventDetails.this.startActivity(intent);
                                         }
                                     }
        );



    }

    private void populateImgString() {
        imgString.add("http://res.cloudinary.com/djh5qwbxp/image/upload/v1436768040/singapore_rqdh0j.jpg");
        imgString.add("http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767999/singapore4_sozhjb.jpg");
        imgString.add("http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767996/singapore3_hx8apl.jpg");
        imgString.add("http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767992/singapore2_z8kwo2.jpg");
        imgString.add("http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767990/orchard5_zm1rmh.jpg");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
        }
        if(id == R.id.country_settings) {
            Intent intent = new Intent(EventDetails.this, LocationFinder.class);
            EventDetails.this.startActivity(intent);
        }
        if(id == R.id.login) {
            Intent intent = new Intent(EventDetails.this, Login.class);
            EventDetails.this.startActivity(intent);
        }
        if(id == R.id.exit_app) {
            System.exit(0);
        }
        return super.onOptionsItemSelected(item);
    }
}
